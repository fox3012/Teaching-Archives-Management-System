package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysFileInfoMapper;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.service.ISysFileInfoService;

/**
 * 文件信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class SysFileInfoServiceImpl implements ISysFileInfoService
{
    @Autowired
    private SysFileInfoMapper sysFileInfoMapper;

    /**
     * 查询文件信息
     *
     * @param fileId 文件信息主键
     * @return 文件信息
     */
    @Override
    public SysFileInfo selectSysFileInfoByFileId(Long fileId)
    {
        return sysFileInfoMapper.selectSysFileInfoByFileId(fileId);
    }

    /**
     * 查询文件信息列表
     *
     * @param sysFileInfo 文件信息
     * @return 文件信息
     */
    @Override
    public List<SysFileInfo> selectSysFileInfoList(SysFileInfo sysFileInfo)
    {
        return sysFileInfoMapper.selectSysFileInfoList(sysFileInfo);
    }

    /**
     * 新增文件信息
     *
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public int insertSysFileInfo(SysFileInfo sysFileInfo)
    {
        return sysFileInfoMapper.insertSysFileInfo(sysFileInfo);
    }

    /**
     * 修改文件信息
     *
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public int updateSysFileInfo(SysFileInfo sysFileInfo)
    {
        return sysFileInfoMapper.updateSysFileInfo(sysFileInfo);
    }

    /**
     * 批量删除文件信息
     *
     * @param fileIds 需要删除的文件信息主键
     * @return 结果
     */
    @Override
    public int deleteSysFileInfoByFileIds(Long[] fileIds)
    {
        return sysFileInfoMapper.deleteSysFileInfoByFileIds(fileIds);
    }

    /**
     * 删除文件信息信息
     *
     * @param fileId 文件信息主键
     * @return 结果
     */
    @Override
    public int deleteSysFileInfoByFileId(Long fileId)
    {
        return sysFileInfoMapper.deleteSysFileInfoByFileId(fileId);
    }

    /**
     * 校验文件名称是否唯一
     *
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public boolean checkFileNameUnique(SysFileInfo sysFileInfo) {

        // 如果文件名为空，则认为文件名不唯一
        if (StringUtils.isEmpty(sysFileInfo.getFileName())) {
            return false;
        }

        // 通过文件名查询数据库中是否已经存在相同文件名的记录
        SysFileInfo fileInfo = sysFileInfoMapper.checkFileNameUnique(sysFileInfo.getFileName());

        // 如果查询结果不为空，并且文件ID不等于当前文件ID（即已经存在文件名相同但文件ID不同的记录），则返回 false
        if (fileInfo != null && !fileInfo.getFileId().equals(sysFileInfo.getFileId())) {
            return false;
        }

        // 如果以上条件都不满足，则说明文件名是唯一的，返回 true
        return true;
    }
    /**
     * 查询当前角色对应的文件信息
     *
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public List<SysFileInfo> selectAllSysFileInfoList(SysFileInfo sysFileInfo) {
        return sysFileInfoMapper.selectAllSysFileInfoList(sysFileInfo);
    }

}
